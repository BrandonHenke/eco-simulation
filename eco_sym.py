import numpy as np
import matplotlib.pyplot as plt


E_lp, E_rp, E_gp = 1, 0.5, 10000
E_lh, E_rh, E_gh = 1, 0.5, 0.9

plants_N = 1000
hoppers_N = 0

rounds = 200


plants = np.zeros(plants_N); plants += E_gp//len(plants)
hoppers = np.zeros(hoppers_N)

N,y,z = [0],[[sum(plants)/(sum(plants)+sum(hoppers))],[sum(hoppers)/(sum(plants)+sum(hoppers))]],[[len(plants)],[len(hoppers)]]

for m in range(1,rounds+1):
	n = 0
	while n < len(hoppers):
		if len(plants) == 0:
			break
		pl = np.random.randint(len(plants))
		hoppers[n] += E_gh * plants[pl] - E_lh
		plants = np.delete(plants,pl)
		if  hoppers[n] >= np.random.normal(E_lh,E_rh):
			n += 1

	plants -= E_lp
	# hoppers -= E_lh

	plants = plants[(plants >= 0)]
	hoppers = hoppers[(hoppers >= 0)]
	reps_p = np.where(plants >= E_rp)[0]
	reps_h = np.where(hoppers >= E_rh)[0]
	if len(plants) == 0:
		plants = np.zeros(10)
	# if len(hoppers) == 0:
	# 	hoppers = np.zeros(10)
	plants[reps_p] -= E_rp
	plants=np.append(plants,np.zeros(len(reps_p)))
	hoppers[reps_h] -= E_rh
	hoppers=np.append(hoppers,np.zeros(len(reps_h)))
	# while len(reps_p) > 0:
	# 	plants[reps_p] -= E_rp
	# 	plants=np.append(plants,np.zeros(len(reps_p)))
	# 	reps_p = np.where(plants >= E_rp)[0]
	# while len(reps_h) > 0:
	# 	hoppers[reps_h] -= E_rh
	# 	hoppers=np.append(hoppers,np.zeros(len(reps_h)))
	# 	reps_h = np.where(hoppers >= E_rh)[0]

	if len(plants) != 0:
		plants += E_gp//len(plants)
	if sum(plants)+sum(hoppers) != 0:
		y[0] += [sum(plants)/(sum(plants)+sum(hoppers))]
		y[1] += [sum(hoppers)/(sum(plants)+sum(hoppers))]
	else:
		y[0] += [0]
		y[1] += [0]

	N += [m]
	z[0] += [len(plants)]
	z[1] += [len(hoppers)]
	print(z[0][-1])
	print(z[1][-1])
	print(m/rounds)
	print()

# print(len(plants))
# print(len(hoppers))
# print(N)
# print(y)
f1 = plt.figure()
plt.title("Energy")
plt.plot(N,y[0],label="plants")
plt.plot(N,y[1],label="hoppers")
plt.legend()
f2 = plt.figure()
plt.title("Population")
plt.plot(N,z[0],label="plants")
plt.plot(N,z[1],label="hoppers")
plt.legend()
plt.show()
